extends RigidBody2D

var screensize = Vector2()
var size
var radius
var scale_factor = 0.5

signal exploded

func start(_position, velocity, _size):
	position = _position
	size = _size
	mass = 1.5 * size
	var new_scale = Vector2(1, 1) * scale_factor * size
	$Sprite.scale = new_scale
	$CollisionShape2D.scale = new_scale
	radius = $CollisionShape2D.shape.radius
	linear_velocity = velocity
	angular_velocity = rand_range(-1.5, 1.5)
	$Explosion.scale = Vector2(0.75, 0.75) * _size

func _integrate_forces(physics_state):
	var state_transform = physics_state.get_transform()
	if state_transform.origin.x > screensize.x + radius:
		state_transform.origin.x = 0 - radius
	if state_transform.origin.y > screensize.y + radius:
		state_transform.origin.y = 0 - radius
	if state_transform.origin.x < 0 - radius:
		state_transform.origin.x = screensize.x + radius
	if state_transform.origin.y < 0 - radius:
		state_transform.origin.y = screensize.y + radius
	physics_state.set_transform(state_transform)

func explode():
	layers = 0
	$Sprite.hide()
	$Explosion/AnimationPlayer.play("explosion")
	emit_signal("exploded", size, radius, position, linear_velocity)
	linear_velocity = Vector2()
	angular_velocity = 0

func _on_AnimationPlayer_animation_finished(_anim_name):
	queue_free()
