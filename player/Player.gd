extends RigidBody2D

enum {INIT, ALIVE, INVULNERABLE, DEAD}
var state = INIT

export (int) var engine_power
export (int) var spin_power

var thrust = Vector2()
var rotation_dir = 0

var screensize = Vector2()

signal shoot
export (PackedScene) var Bullet
export (float) var fire_rate

var can_shoot = false

var radius

signal lives_changed
var lives = 0 setget set_lives

signal dead

signal shield_changed

export (int) var max_shield
export (float) var shield_regen

var shield = 0 setget set_shield

signal hit

func _ready():
	change_state(INIT)
	$GunTimer.wait_time = fire_rate
	radius = $CollisionShape2D.shape.radius

func change_state(new_state):
	match new_state:
		INIT:
			$CollisionShape2D.set_deferred("disabled", true)
			$Sprite.modulate.a = 0.5
		ALIVE:
			$CollisionShape2D.set_deferred("disabled", false)
			$Sprite.modulate.a = 1.0
		INVULNERABLE:
			$CollisionShape2D.set_deferred("disabled", true)
			$Sprite.modulate.a = 0.5
			$InvulnerabilityTimer.start()
		DEAD:
			$CollisionShape2D.set_deferred("disabled", true)
			$Sprite.hide()
			linear_velocity = Vector2()
			angular_velocity = 0
			emit_signal("dead")
			$EngineSound.stop()
	state = new_state

func _process(delta):
	get_input()
	if not state == INIT:
		self.shield += shield_regen * delta

func get_input():
	$Exhaust.emitting = false
	thrust = Vector2()
	if state in [INIT, DEAD]:
		return
	if Input.is_action_pressed("thrust"):
		thrust = Vector2(engine_power, 0)
		if not $EngineSound.playing:
			$EngineSound.play()
		$Exhaust.emitting = true
	else:
		$EngineSound.stop()
		$Exhaust.emitting = false
	rotation_dir = 0
	if Input.is_action_pressed("rotate_right"):
		rotation_dir += 1
	if Input.is_action_pressed("rotate_left"):
		rotation_dir -= 1
	if Input.is_action_pressed("shoot") and can_shoot:
		shoot()

func _integrate_forces(physics_state):
	set_applied_force(thrust.rotated(rotation))
	set_applied_torque(spin_power * rotation_dir)
	var state_transform = physics_state.get_transform()
	if state == INIT:
		state_transform = Transform2D(0, screensize/2)
	if state_transform.origin.x > screensize.x + radius:
		state_transform.origin.x = 0 - radius
	if state_transform.origin.y > screensize.y + radius:
		state_transform.origin.y = 0 - radius
	if state_transform.origin.x < 0 - radius:
		state_transform.origin.x = screensize.x + radius
	if state_transform.origin.y < 0 - radius:
		state_transform.origin.y = screensize.y + radius
	physics_state.set_transform(state_transform)

func shoot():
	if state == INVULNERABLE:
		return
	emit_signal("shoot", Bullet, $Muzzle.global_position, rotation)
	can_shoot = false
	$GunTimer.start()
	$LaserSound.play()

func _on_GunTimer_timeout():
	can_shoot = true

func set_lives(value):
	lives = value
	self.shield = max_shield
	emit_signal("lives_changed", lives)
	if lives <= 0:
		change_state(DEAD)
	else:
		change_state(INVULNERABLE)

func start():
	$Sprite.show()
	self.lives = 3
	change_state(ALIVE)
	self.shield = max_shield

func _on_InvulnerabilityTimer_timeout():
	change_state(ALIVE)

func _on_AnimationPlayer_animation_finished(_anim_name):
	$Explosion.hide()

func _on_Player_body_entered(body):
	if body.is_in_group("rocks"):
		body.explode()
		$Explosion.show()
		$Explosion/AnimationPlayer.play("explosion")
		self.shield -= body.size * 25
		take_damage()

func set_shield(value):
	if value > max_shield:
		value = max_shield
	shield = value
	emit_signal("shield_changed", shield)
	if shield <= 0:
		self.lives -= 1

func take_damage():
	emit_signal("hit")
