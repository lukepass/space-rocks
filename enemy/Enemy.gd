extends Area2D

signal shoot

export (PackedScene) var Bullet
export (int) var speed
export (int) var health

var follow
var target = null

func _ready():
	$Sprite.frame = randi() % 3
	var path = $EnemyPaths.get_children()[randi() % $EnemyPaths.get_child_count()]
	follow = PathFollow2D.new()
	path.add_child(follow)
	follow.loop = false

func _process(delta):
	follow.offset += speed * delta
	position = follow.global_position
	if follow.unit_offset >= 1:
		queue_free()

func _on_AnimationPlayer_animation_finished(_anim_name):
	queue_free()

func _on_GunTimer_timeout():
	shoot_pulse(3, 0.25)

func shoot():
	var direction = target.global_position - global_position
	direction = direction.rotated(rand_range(-0.1, 0.1)).angle()
	emit_signal('shoot', Bullet, global_position, direction)

func shoot_pulse(n, delay):
	for _i in range(n):
		$ShootSound.play()
		shoot()
		yield(get_tree().create_timer(delay), "timeout")

func take_damage(amount):
	health -= amount
	$AnimationPlayer.play("flash")
	if health <= 0:
		explode()
	yield($AnimationPlayer, "animation_finished")
	$AnimationPlayer.play("rotate")

func explode():
	$ExplodeSound.play()
	$GunTimer.stop()
	$CollisionShape2D.set_deferred("disabled", true)
	$Sprite.hide()
	$Explosion.show()
	$Explosion/AnimationPlayer.play("explosion")

func _on_Enemy_body_entered(body):
	if body.name == "Player":
		body.shield -= 50
		body.take_damage()
		# TODO mettendo explode fuori dall'if i nemici sono danneggiati dalle rocce
		explode()

func enlarge(_scale = 2):
	speed /= _scale
	health *= _scale
	$Sprite.scale *= _scale
	$CollisionShape2D.scale *= _scale
	$ShootSound.pitch_scale /= _scale
	$Explosion.scale *= _scale
