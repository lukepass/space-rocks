extends Node

export (PackedScene) var Rock

var screensize = Vector2()

var level = 0
var score = 0
var multiplier = 1.0
var playing = false

export (PackedScene) var Enemy

func _ready():
	randomize()
	screensize = get_viewport().get_visible_rect().size
	$Player.screensize = screensize

func spawn_rock(size, _position = null, velocity = null):
	if !_position:
		$RockPath/RockSpawn.set_offset(randi())
		_position = $RockPath/RockSpawn.position
	if !velocity:
		velocity = Vector2(1, 0).rotated(rand_range(0, 2 * PI)) * rand_range(100, 150)
	var r = Rock.instance()
	r.screensize = screensize
	r.start(_position, velocity, size)
	$Rocks.add_child(r)
	r.connect("exploded", self, "_on_Rock_exploded")

func _on_Player_shoot(bullet, _position, _rotation):
	var b = bullet.instance()
	b.start(_position, _rotation)
	$Bullets.add_child(b)

func _on_Rock_exploded(size, radius, _position, velocity):
	score += size * 10 * multiplier
	multiplier += 0.1
	$HUD.update_score(score, multiplier)
	$ExplodeSound.play()
	if size <= 1:
		return
	for offset in [-1, 1]:
		var dir = (_position - $Player.position).normalized().tangent() * offset
		var new_position = _position + dir * radius
		var new_velocity = dir * velocity.length() * 1.1
		call_deferred("spawn_rock", size - 1, new_position, new_velocity)

func new_game():
	$Music.play()
	for rock in $Rocks.get_children():
		rock.queue_free()
	for enemy in $Enemies.get_children():
		enemy.queue_free()
	level = 0
	score = 0
	multiplier = 1.0
	$HUD.update_score(score, multiplier)
	$Player.start()
	$HUD.show_message("Get Ready!")
	yield($HUD/MessageTimer, "timeout")
	playing = true
	new_level()

func new_level():
	$LevelupSound.play()
	level += 1
	if level % 5 == 0:
		$HUD.show_message("Warning!")
		for _i in range(level):
			var e = Enemy.instance()
			$Rocks.add_child(e)
			e.target = $Player
			e.connect("shoot", self, "_on_Player_shoot")
			e.enlarge()
			yield(get_tree().create_timer(rand_range(1, 3)), "timeout")
	else:
		$HUD.show_message("Wave %s" % level)
		for _i in range(level):
			spawn_rock(3)
		$EnemyTimer.wait_time = rand_range(5, 10)
		$EnemyTimer.start()

func _process(_delta):
	if playing and $Rocks.get_child_count() == 0:
		new_level()

func game_over():
	$Music.stop()
	playing = false
	$HUD.game_over()

func _input(event):
	if event.is_action_pressed("pause"):
		if not playing:
			return
		get_tree().paused = !get_tree().paused
		if get_tree().paused:
			$HUD/MessageLabel.text = "Paused"
			$HUD/MessageLabel.show()
		else:
			$HUD/MessageLabel.text = ""
			$HUD/MessageLabel.hide()

func _on_EnemyTimer_timeout():
	var e = Enemy.instance()
	$Enemies.add_child(e)
	e.target = $Player
	e.connect("shoot", self, "_on_Player_shoot")
	$EnemyTimer.wait_time = rand_range(20, 40)
	$EnemyTimer.start()

func _on_Player_shield_changed(value):
	$HUD.update_shield(value)

func _on_Player_hit():
	multiplier = 1.0
	$HUD.update_score(score, multiplier)
